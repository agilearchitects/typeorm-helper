import { Connection, ConnectionManager, ConnectionOptions, DefaultNamingStrategy, EntityManager, NamingStrategyInterface } from "typeorm";
import { RelationIdLoader } from "typeorm/query-builder/RelationIdLoader";
import { RelationLoader } from "typeorm/query-builder/RelationLoader";

export type relationLoaderModule<T extends Connection = Connection> = new (connection: T) => RelationLoader;
export type relationIdLoaderModule<T extends Connection = Connection> = new (connection: T) => RelationIdLoader;
export class ConnectionManangerModule {
  public constructor(
    private readonly getConnectionManagerMethod: () => ConnectionManager,
    private readonly relationLoaderModule: relationLoaderModule,
    private readonly relationIdLoaderModule: relationIdLoaderModule,
  ) { }

  public async connect(options: ConnectionOptions): Promise<Connection> {
    // Get typeorm connection manager
    const connectionManager = this.getConnectionManagerMethod();

    // Get connection name from options (will default to "default");
    const connectionName = "name" in options && options.name !== undefined ? options.name : "default";

    // Check if a default connection already exists
    if (connectionManager.has(connectionName)) {
      /* If connection exists, injects connection options
      (will replace existing connection options) */
      const connection = this.injectConnectionOptions(
        connectionManager.get(connectionName),
        options
      );

      // Check if connection is esablished
      if (connection.isConnected === false) {
        // If no connection is established: Connect
        await connection.connect();
      }

      // Return connection
      return connection;
    } else {
      /* No connection could be found. Create (with options)
      and connect */
      return connectionManager.create(options).connect();
    }
  }

  public async enclose(options: ConnectionOptions, callback: (connection: Connection) => Promise<void>): Promise<void> {
    const connection = await this.connect(options);
    try {
      await callback(connection);
    } finally {
      connection.close();
    }
  }

  private injectConnectionOptions(connection: Connection, connectionOptions: ConnectionOptions) {
    /**
     * from Connection constructor()
     */

    (connection.options as ConnectionOptions) = connectionOptions;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ((connection as any).manager as EntityManager) = connection.createEntityManager();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ((connection as any).namingStrategy as NamingStrategyInterface) = connection.options.namingStrategy || new DefaultNamingStrategy();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ((connection as any).relationLoader as RelationLoader) = new this.relationLoaderModule(connection);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ((connection as any).relationIdLoader as RelationIdLoader) = new this.relationIdLoaderModule(connection);
    
    /**
     * from Connection connect()
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ((connection as any).buildMetadatas as () => void)();
    return connection;
  }
}