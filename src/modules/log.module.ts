import { LogModule, ILogMessage } from "@agilearchitects/logmodule";
import { QueryRunner, Logger } from "typeorm";

/**
 * Helper method for parsing typeorm logging to @agilearchitects/logmodule
 * @param log LogModule to use when logging from typeorm
 */
export const logHandler = (log: LogModule): Logger => {
  const connectionData = (queryRunner: QueryRunner) => ({
    name: queryRunner.connection.name,
    type: queryRunner.connection.options.type.toString()
  });
  
  return {
    logQuery: (query: string, parameters?: unknown[], queryRunner?: QueryRunner): void => {
      log.info({
        message: "Typeorm executed query",
        data: {
          query,
          ...(parameters !== undefined ? { parameters } : undefined),
          ...(queryRunner !== undefined ? { connection: connectionData(queryRunner) } : undefined)
        }
      });
    },
    logQueryError: (error: string, query: string, parameters?: unknown[], queryRunner?: QueryRunner): void => {
      log.error({
        message: "Typeorm encountered and error",
        data: {
          error,
          query,
          ...(parameters !== undefined ? { parameters } : undefined),
          ...(queryRunner !== undefined ? { connection: queryRunner.connection } : undefined)
        }
      });
    },
    logQuerySlow: (time: number, query: string, parameters?: unknown[], queryRunner?: QueryRunner): void => {
      log.warning({
        message: "Typeorm execution ran longer than expected",
        data: {
          time,
          query,
          ...(parameters !== undefined ? { parameters } : undefined),
          /* There seems to be a bug in typeorm where connection is
          undefined in queryRunner when it shouldn't*/
          ...(queryRunner !== undefined && queryRunner.connection !== undefined ? { connection: connectionData(queryRunner) } : undefined)
        }
      });
    },
    logSchemaBuild: (message: string, queryRunner?: QueryRunner): void => {
      log.info({
        message,
        data: {
          message,
          ...(queryRunner !== undefined ? { connection: connectionData(queryRunner) } : undefined)
        }
      });
    },
    logMigration: (message: string, queryRunner?: QueryRunner): void => {
      log.info({
        message,
        data: {
          message,
          ...(queryRunner !== undefined ? { connection: connectionData(queryRunner) } : undefined)
        }
      });
    },
    log: (level: "log" | "info" | "warn", message: string, queryRunner?: QueryRunner): void => {
      const logMessage: ILogMessage = {
        message,
        data: {
          message,
          ...(queryRunner !== undefined ? { connection: connectionData(queryRunner) } : undefined)
        }
      };
      if (level === "log") { log.log(logMessage); }
      else if (level === "info") { log.info(logMessage); }
      else if (level === "warn") { log.warning(logMessage); }
    },
  };
};