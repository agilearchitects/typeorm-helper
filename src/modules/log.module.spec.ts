import { LogModule, logLevel, ILog } from "@agilearchitects/logmodule";
import { expect } from "chai";
import { describe, it } from "mocha";
import { getConnectionManager, Connection, Logger } from "typeorm";
import { RelationIdLoader } from "typeorm/query-builder/RelationIdLoader";
import { RelationLoader } from "typeorm/query-builder/RelationLoader";

// Modules
import { ConnectionManangerModule } from "./connection-manager.module";
import { logHandler } from "./log.module";

const connect = async (logger: Logger): Promise<Connection> => {
  const connectionManager = new ConnectionManangerModule(getConnectionManager, RelationLoader, RelationIdLoader);
  return connectionManager.connect({
    name: Math.random().toString(),
    type: "sqlite",
    database: ":memory:",
    logger
  });
};

describe("LogModule", () => {
  it("Should be able to handle query logging", async () => {
    const connection = await connect(logHandler(new LogModule([{
      level: logLevel.VERBOSE,
      handler: (log: ILog) => {
        expect(log.level).equal(logLevel.INFO);
        expect(log.data).not.undefined;
        expect(log.data).have.property("query");
        expect(log.data).have.property("connection");
      }
    }])));
    await connection.query("Select 1");
    await connection.close();
  });

  it("Should be able to handle query error logging", async () => {
    const connection = await connect(logHandler(new LogModule([{
      level: logLevel.VERBOSE,
      handler: (log: ILog) => {
        if (log.level === logLevel.ERROR) {
          expect(log.data).not.undefined;
          expect(log.data).have.property("query");
          // Typeorm bug prevents us for verifying that connection is defined
          // expect(log.data).have.property("connection");
        }
      }
    }])));
    try {
      await connection.query("Select");
      await connection.close();
    } catch { /** */ }
  });
});