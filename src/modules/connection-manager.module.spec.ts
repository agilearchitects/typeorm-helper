// Libs
import { expect } from "chai";
import { describe, it } from "mocha";
import { getConnectionManager } from "typeorm";
import { RelationIdLoader } from "typeorm/query-builder/RelationIdLoader";
import { RelationLoader } from "typeorm/query-builder/RelationLoader";

// Modules
import { ConnectionManangerModule } from "../modules/connection-manager.module";

describe("ConnectionManagerModule", () => {
  it("Should be able to connect to DB", async () => {
    const connectionManager = new ConnectionManangerModule(getConnectionManager, RelationLoader, RelationIdLoader);
    const connection = await connectionManager.connect({ type: "sqlite", database: ":memory:" });
    await connection.query("SELECT 1");
    await connection.close();
  });

  it("Should be able to reconnect to DB", async () => {
    const connectionManager = new ConnectionManangerModule(getConnectionManager, RelationLoader, RelationIdLoader);
    const connection = await connectionManager.connect({ type: "sqlite", database: ":memory:" });
    await connection.close();
    const connection2 = await connectionManager.connect({ type: "sqlite", database: ":memory:" });
    await connection2.close();
  });

  it("Should be able to fetch the same running connection", async () => {
    const connectionManager = new ConnectionManangerModule(getConnectionManager, RelationLoader, RelationIdLoader);
    const connection = await connectionManager.connect({ type: "sqlite", database: ":memory:" });
    const connection2 = await connectionManager.connect({ type: "sqlite", database: ":memory:" });
    expect(connection).equal(connection2);
    await connection2.close();
  });
});