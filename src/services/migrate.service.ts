// Libs
import { Connection, Migration, MigrationExecutor } from "typeorm";
import { MysqlDriver } from "typeorm/driver/mysql/MysqlDriver";

export interface IMigration extends Migration {
  executed: boolean;
}

export interface IGeneratedMigration {
  timestamp: string;
  content: string;
}

export class MigrateService {
  public constructor(
    private readonly connection: Connection,
  ) { }

  public async migrate(): Promise<void> {
    await this.connection.runMigrations({ transaction: "all" });
  }

  public async rollback(): Promise<void> {
    await this.connection.undoLastMigration({ transaction: "all" });
  }

  public async show(): Promise<IMigration[]> {
    this.connection.createQueryRunner();
    const migrationExecutor = new MigrationExecutor(this.connection, this.connection.createQueryRunner());
    const executedMigrations = await migrationExecutor.getExecutedMigrations();

    return (await migrationExecutor.getAllMigrations()).map((migration: Migration) => ({
      ...migration,
      executed: executedMigrations.findIndex((executedMigration: Migration) => executedMigration.name === migration.name) !== -1,
    }));
  }

  public async generate(): Promise<IGeneratedMigration | undefined> {
    const sqlInMemory = await this.connection.driver.createSchemaBuilder().log();
    const upSqls: string[] = [];
    const downSqls: string[] = [];
    // mysql is exceptional here because it uses ` character in to escape names in queries, that's why for mysql
    // we are using simple quoted string instead of template string syntax
    if (this.connection.driver instanceof MysqlDriver) {
      sqlInMemory.upQueries.forEach((query) => {
        upSqls.push("        await queryRunner.query(\"" + query.query.replace(new RegExp("\"", "g"), "\\\"") + "\");");
      });
      sqlInMemory.downQueries.forEach((query) => {
        downSqls.push("        await queryRunner.query(\"" + query.query.replace(new RegExp("\"", "g"), "\\\"") + "\");");
      });
    } else {
      sqlInMemory.upQueries.forEach((query) => {
        upSqls.push("        await queryRunner.query(`" + query.query.replace(new RegExp("`", "g"), "\\`") + "`);");
      });
      sqlInMemory.downQueries.forEach((query) => {
        downSqls.push("        await queryRunner.query(`" + query.query.replace(new RegExp("`", "g"), "\\`") + "`);");
      });
    }

    if (upSqls.length) {
      const timestamp = this.timestamp();
      return {
        timestamp,
        content:  "import { MigrationInterface, QueryRunner } from \"typeorm\";\n" +
        "\n" +
        `// tslint:disable-next-line: class-name\nexport class _${timestamp} implements MigrationInterface {\n` +
        "\n" +
        "    public async up(queryRunner: QueryRunner): Promise<any> {\n" +
        `${upSqls.join("\n")}\n` +
        "    }\n" +
        "\n" +
        "    public async down(queryRunner: QueryRunner): Promise<any> {\n" +
        `${downSqls.reverse().join("\n")}\n` +
        "    }\n" +
        "\n" +
        "}\n"
      };
    }
  }

  private timestamp(): string {
    const padLeft = (input: number, length: number, char: string = "0"): string => {
      return `${char.repeat(length - Math.max(input.toString().length, 0))}${input}`;
    };

    const date = new Date();

    return `${date.getFullYear()}${padLeft(date.getMonth() + 1, 2)}${padLeft(date.getDate(), 2)}_${padLeft(date.getHours(), 2)}${padLeft(date.getMinutes(), 2)}${padLeft(date.getSeconds(), 2)}`;
  }
}
