// Libs
import { describe, it } from "mocha";
import { getConnectionManager, Connection } from "typeorm";
import { MigrationInterface, QueryRunner } from "typeorm";
import { RelationIdLoader } from "typeorm/query-builder/RelationIdLoader";
import { RelationLoader } from "typeorm/query-builder/RelationLoader";

// Modules
import { ConnectionManangerModule } from "../modules/connection-manager.module";

// Services
import { MigrateService } from "./migrate.service";


describe("MigrateService", () => {
  it("Should be able to create", async () => {
    const connectionManager = new ConnectionManangerModule(getConnectionManager, RelationLoader, RelationIdLoader);
    connectionManager.enclose({ type: "sqlite", database: ":memory:" }, async (connection: Connection): Promise<void> => {
      new MigrateService(connection);
    });
  });
  it("Should be able to run migrations", async () => {
    class _20200625_094039 implements MigrationInterface {

      public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE \"myTable\" (\"id\" integer PRIMARY KEY AUTOINCREMENT NOT NULL, \"createdAt\" datetime NOT NULL DEFAULT (datetime('now')), \"updatedAt\" datetime NOT NULL DEFAULT (datetime('now')))");
      }
      public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP TABLE \"myTable\"");
      }
    }
    const connectionManager = new ConnectionManangerModule(getConnectionManager, RelationLoader, RelationIdLoader);
    connectionManager.enclose({
      type: "sqlite", database: ":memory:", migrations: [
        _20200625_094039
      ]
    }, async (connection: Connection): Promise<void> => {
      const migrationService = new MigrateService(connection);
      await migrationService.migrate();
    });
  });
});